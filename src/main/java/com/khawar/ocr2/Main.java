/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.khawar.ocr2;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import net.sourceforge.tess4j.Tesseract;
import net.sourceforge.tess4j.TesseractException;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.PDPageContentStream.AppendMode;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.rendering.ImageType;
import org.apache.pdfbox.rendering.PDFRenderer;
import org.opencv.core.Core;
import static org.opencv.core.Core.BORDER_CONSTANT;
import static org.opencv.core.Core.copyMakeBorder;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
//import org.opencv.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;

/**
 *
 * @author User
 */
public class Main {

    public static void main(String[] args) {
        try {
            nu.pattern.OpenCV.loadShared();
            //   nu.pattern.OpenCV.loadShared();
            String pdfFile = "data/Kassenbeleg/Dok 03. Nov. 2017, 09_34.pdf";

//            Tesseract tes = new Tesseract();
//            tes.setLanguage("eng");
//            tes.setDatapath("F:\\zero meter\\OCRtool\\tessdata");
//            String str = tes.doOCR(new File(pdfFile));
//            System.out.println(str);
//            if(true)return;
            BufferedImage image = null;
            float width = 0;
            float height = 0;
            if (pdfFile.toLowerCase().endsWith(".pdf")) {
                // mygui.AddLog("Processing file: " + pdfFile);
                PDDocument doc = PDDocument.load(new File(pdfFile));

                PDFRenderer pdfRenderer = new PDFRenderer(doc);

                PDPage page = (PDPage) doc.getDocumentCatalog().getPages().get(0);
                width = page.getMediaBox().getWidth();
                height = page.getMediaBox().getHeight();
                image = pdfRenderer.renderImageWithDPI(0, 250, ImageType.RGB);
                System.out.println("img w: " + image.getWidth());
                doc.close();
            } else if (pdfFile.toLowerCase().endsWith(".png") || pdfFile.toLowerCase().endsWith(".jpg") || pdfFile.toLowerCase().endsWith(".tiff")) {
                image = ImageIO.read(new File(pdfFile));
                width = image.getWidth() * 2 / 3;
                height = image.getHeight() * 2 / 3;
            }
            Tesseract tes = new Tesseract();
            tes.setLanguage("eng");
            //tes.setOcrEngineMode();
           // tes.setPageSegMode(7);
            tes.setDatapath("F:\\zero meter\\OCRtool\\tessdata");
            //String str = tes.doOCR(new File("test.png"));
            // System.out.println(str);
            PDDocument outDoc = new PDDocument();
            PDPage outPage = new PDPage(new PDRectangle(width, height));
            outDoc.addPage(outPage);
            //float ratio=(float)image.getWidth()/(float)width;
          //  extractFields(image, tes, outDoc, outPage);
            outDoc.save("output.pdf");
        } catch (Exception ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void extractFields(BufferedImage imgFile, Tesseract tess, PDDocument doc, PDPage page) {

        // System.out.println(template.fields);
        Mat mat = image2Mat(imgFile);//Highgui.imread(imgFile, Highgui.CV_LOAD_IMAGE_GRAYSCALE);
        int width = mat.width();
        int height = mat.height();

        float pwidth = page.getMediaBox().getWidth();

        float pheight = page.getMediaBox().getHeight();

        float ratio = ((float) width) / pwidth;

//        Point center = new Point(height / 2, width / 2);
//        Mat rotImage = Imgproc.getRotationMatrix2D(center, 90, 1.0);
//
//        Size size = new Size(height, width);
//
//        Imgproc.warpAffine(mat, mat, rotImage, size);
//
//        Highgui.imwrite("rotated.png", mat);
        //  Mat grey = new Mat();
        Imgproc.cvtColor(mat, mat, Imgproc.COLOR_BGR2GRAY);
        Mat bw = new Mat();
        Imgproc.threshold(mat, bw, 127, 255, Imgproc.THRESH_BINARY_INV);

        Mat element = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(width / 50, 2));
        Imgproc.dilate(bw, bw, element);
        //Imgcodecs.imwrite("bw0.png", bw);
        Imgproc.erode(bw, bw, element);

        Imgcodecs.imwrite("bw.png", bw);
        List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
        Imgproc.findContours(bw.clone(), contours, new Mat(), Imgproc.RETR_TREE, Imgproc.CHAIN_APPROX_SIMPLE);
        contours.sort(new sortbyy());
        // Collections.sort(contours, new sortbyy());
        // ArrayList< MatOfPoint> best = new ArrayList();
        int counter = 0;
        for (int i = 0; i < contours.size(); i++) {
            MatOfPoint cnt = contours.get(i);
            Rect rect = Imgproc.boundingRect(cnt);

            MatOfPoint2f approx = new MatOfPoint2f(cnt.toArray());
            Imgproc.approxPolyDP(approx, approx, Imgproc.arcLength(approx, true) * 0.02, true);

            int cwidth = 30;
            int cheight = 10;
            int carea = 1000;

            if (rect.width > 10 && rect.height > 5 && rect.height < height / 3) {
                counter++;
                String text = "";
                {

                    Mat crop = new Mat(mat, new Rect(rect.x, rect.y, rect.width, rect.height));
                    // crop = toBW(crop);
                    Core.copyMakeBorder(crop, crop, 4, 4, 4, 4, Core.BORDER_CONSTANT, new Scalar(255));
                    //  Highgui.imwrite("crop/crop_" + counter + ".png", crop);
                    BufferedImage image = Mat2Image(crop);
                    try {
                        ImageIO.write(image, "png", new File("tmp.png"));
                        text = tess.doOCR(new File("tmp.png")).trim();
//                        template.processFields(text, mygui);
                        System.out.println(text);
                        if (text.length() > 0) {
                            drawText(doc, page, text, (int) ((float) rect.x / ratio), (int) ((float) rect.y / ratio));
                        }

                    } catch (Exception ex) {
                        Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }

                // slist.add(text);
            }
        }

    }

    public static void drawText(PDDocument d, PDPage page, String str, int x, int y) {
        try {
            PDFont font = PDType1Font.HELVETICA_BOLD;
            PDPageContentStream contents = new PDPageContentStream(d, page, AppendMode.APPEND, false);
            contents.beginText();
            // contents.setTextRotation(0, x, y);
            contents.newLineAtOffset(x, y);
            // contents.setTextRotation(90, 0, 0);
            // contents.setTextMatrix(new rota);
            contents.setFont(font, 12);
            contents.setNonStrokingColor(Color.red);
            // contents.moveTextPositionByAmount(30, 30);
            contents.showText(str);
            contents.endText();

            contents.close();
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public static Mat image2Mat(BufferedImage img) {
        BufferedImage screen = toBufferedImageOfType(img, BufferedImage.TYPE_3BYTE_BGR);
        byte[] pixels = ((DataBufferByte) screen.getRaster().getDataBuffer()).getData();
        Mat mat = new Mat(screen.getHeight(), screen.getWidth(), CvType.CV_8UC3);
        mat.put(0, 0, pixels);
        return mat;
    }

    public static BufferedImage toBufferedImageOfType(BufferedImage original, int type) {
        if (original == null) {
            throw new IllegalArgumentException("original == null");
        }

        // Don't convert if it already has correct type
        if (original.getType() == type) {
            return original;
        }

        // Create a buffered image
        BufferedImage image = new BufferedImage(original.getWidth(), original.getHeight(), type);

        // Draw the image onto the new buffer
        Graphics2D g = image.createGraphics();
        try {
            g.setComposite(AlphaComposite.Src);
            g.drawImage(original, 0, 0, null);
        } finally {
            g.dispose();
        }

        return image;
    }

    public static BufferedImage Mat2Image(Mat mat) {
        BufferedImage out = new BufferedImage(mat.width(), mat.height(), BufferedImage.TYPE_BYTE_GRAY);

// Get the BufferedImage's backing array and copy the pixels directly into it
        byte[] data = ((DataBufferByte) out.getRaster().getDataBuffer()).getData();
        mat.get(0, 0, data);
        return out;
    }

    public static class sortbyy implements Comparator<MatOfPoint> {

        @Override
        public int compare(MatOfPoint o1, MatOfPoint o2) {
            return Imgproc.boundingRect(o1).y - Imgproc.boundingRect(o2).y;
        }
    }
}
